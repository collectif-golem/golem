#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Golem info"
html_title = project
html_logo = "_static/logo.png"
author = "Human"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

today=version

extensions = [
    "sphinx.ext.intersphinx",
]

intersphinx_mapping = {
    "guerrieres": ("https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/", None),
    "chinsky": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "conflitpal": ("https://conflits.frama.io/israel-palestine/israel-palestine-2023/", None),
    "sionisme": ("https://israel.frama.io/sionisme-antisionisme/", None),
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "raar_2023": ("https://antiracisme.frama.io/infos-2023", None),    
    "raar_2024": ("https://antiracisme.frama.io/infos-2024", None),    
}
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions += ["sphinx.ext.todo"]
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "http://collectif-golem.frama.io/golem",
    "repo_url": "https://framagit.org/collectif-golem/golem",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "deep-purple",
    "color_accent": "blue",
    "theme_color": "red",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },    
        {
            "href": "https://antiracisme.frama.io/linkertree/",
            "internal": False,
            "title": "Liens antiracisme",
        },
    ],
    "heroes": {
        "index": "Golem",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True
extensions += ["sphinxcontrib.youtube"]

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |AlbertHerszkowicz| image:: /images/albert_herszkowicz_avatar.png
.. |FabienneMessica| image:: /images/fabienne_messica_avatar.png
.. |Pornographes| image:: /images/pornographes_avatar.png
.. |Nadine Herrati| image:: /images/nadine_herrati.jpg
.. |Nathalie Fromont| image:: /images/nathalie_fromont.png
.. |Gregory Gutierez| image:: /images/gregory_gutierez.jpg
.. |Nathan Guedj| image:: /images/nathan_guedj.jpg
.. |logo| image:: /images/logo_avatar.png
.. |jjr| image:: /images/jjr_avatar.png
.. |raar| image:: /images/raar_avatar.png
.. |ldh| image:: /images/ldh_avatar.png
.. |halte| image:: /images/halte_antisemitisme_avatar.jpg
.. |eelv| image:: /images/eelv_avatar.png
.. |memorial98| image:: /images/memorial_98_avatar.png
.. |golem| image:: /images/golem_avatar.png
.. |nadav| image:: /images/nadav_avatar.png
.. |RobertHirsch| image:: /images/robert_hirsch_avatar.png
.. |JuifsAllemands| image:: /images/juifs_allemands_avatar.png
.. |GaucheEtJuives| image:: /images/la_gauche_et_les_juifs_avatar.png
.. |SudEducation| image:: /images/sud_education_avatar.png
.. |NonnaMayer| image:: /images/nonna_mayer_avatar.png
.. |CNCDH| image:: /images/cncdh_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
