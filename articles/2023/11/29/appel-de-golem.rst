.. index::
   pair: Appel ; Golem (2023-11-29)

.. _golem_2023_11_29:

===================================================
2023-11-29 **Appel du collectif Golem** |golem|
===================================================

- https://piaille.fr/@collectifgolem/111498882975751947
- https://www.instagram.com/p/C0Q7BFfgIAF/

#appel #texte #politique #lutte #antisemitism #antisemitisme #golem #antiracisme #islamophobie
#jewdiverse #mazeldon

Historique du golem
==========================

- https://fr.wikipedia.org/wiki/Golem

Au XVIe siècle, les juif·ves de Prague étaient la cible de pogroms. 

Une légende raconte que le maharal, rabbin de la ville, sculpta un géant dans
la terre et grava les lettres אםת (vérité) sur son front. 
Alors, le Golem s’anima pour **repousser les attaques antijuives**. 

**Nous le ré-invoquons aujourd’hui, mu·es par l’impératif de combattre l’antisémitisme, 
et par la nécessité d’apaiser la situation politique en redonnant sens aux mots**.

Le collectif Golem est né d’une volonté de rassembler les juif·ves de gauche, 
dont les voix singulières ne trouvaient plus où s’exprimer. 

Nos intentions sont **triples** :

1) **Faire reculer l’extrême droite**
==========================================

Faire reculer l’extrême droite **et rappeler que l’antisémitisme lui
est consubstantiel**. 

Rappeler sans relâche que son prétendu engagement dans la lutte contre 
l’antisémitisme n’est qu’un opportunisme pour dérouler sa haine des immigré·es, 
des Noir·es, des Arabes, des musulman·es, des voyageur·euses, des personnes 
d’origine asiatique, et plus généralement de toutes celles et tous ceux 
qu’elle identifie comme "étrangers·ères ».


2) **Combattre l’antisémitisme**
=======================================

Combattre l’antisémitisme, ses minimisations, son déni et ses
justifications, d’où qu’il vienne, **y compris à gauche**.

3) **Réunir les juif·ves de gauche et leurs soutiens** 
============================================================

déterminé·es à lutter,sans concession aucune, contre l’antisémitisme, dans la 
joie et la fête, le soutien mutuel et la solidarité.

Notre antiracisme refuse de mettre en concurrence les souffrances, de désigner
les populations juives, arabes ou musulmanes comme des "victimes coupables" 
des événements au Proche-Orient et ailleurs. 

**Notre antiracisme refuse de mettre en opposition** la lutte contre **l’islamophobie**
avec celle contre **l’antisémitisme**. 

Notre lutte contre l’antisémitisme participe au combat commun contre tous les 
racismes, pour la justice sociale et contre toute forme de discrimination. 

**Nous voulons remplacer le soupçon par la solidarité**
=============================================================

Constitué à la hâte face à l’urgence, Golem a généré un **enthousiasme
puissant** et dépassé nos espoirs. 

Il doit permettre aux juif·ves de garder ou retrouver leur place dans le 
mouvement social et ouvrier. 
Son travail doit permettre aux gauches de débattre des termes "sionisme » 
et "antisionisme" en les décorrélant de leurs présupposés idéologiques et de 
leur emploi disqualifiant, et d’aborder le conflit israélo-palestinien en militant
pour une résolution juste et pacifique. 

Pour que, enfin, les gauches puissent **discerner la critique légitime de la 
politique coloniale israélienne, de la diabolisation d’une entité imaginaire 
répulsive**.

Populaire, intergénérationnel, familial, inclusif, Golem a pour objet
principal **d’organiser des actions militantes, de contribuer à assainir le
débat public et d’informer sur l’antisémitisme**. 

Il est la maison des juif·ves de gauche et de leurs soutiens qui souhaitent 
se retrouver dans un lieu accueillant pour comprendre et affronter l’antisémitisme 
d’où qu’il vienne. 

Toutes les personnes désireuses de s’investir dans cette
lutte et qui se reconnaissent dans cet appel sont bienvenues. 

Suivez le Golem !
