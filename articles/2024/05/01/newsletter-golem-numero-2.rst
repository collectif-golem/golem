.. index::
   ! Newsletter golem numéro 2 (2024-05-01)

.. _golem_2024_05_01:

=======================================================
2024-05-01 **Newsletter golem numéro 2** |golem|
=======================================================

:download:`Télécharger la lettre N°2 au format PDF <pdfs/newsletter-golem-numero-2-2024-05-01.pdf>`


A vos agendas
================

- 23 mai 2024 à 19h à la Bourse du Travail : Meeting "Qui a peur de la lutte contre l'antisémitisme ? Pour une gauche réellement antiraciste"
  organisé par Golem, Juives et juifs révolutionnaires et le RAAR.

  Inscription par mail : lagauchecontrelantisemitisme@gmail.com
- 26 mai à 20h en visio : Rencontre avec les nouveaux membres (ouvert à tous les membres) *
  Lien pour se connecter: https://meet.google.com/txf-jfpp-sfv?authuser=1&ijlm=1715631837774
- Conférence publique par Jonas Pardo : `Lien d’inscription : https://docs.google.com/forms/d/1e7fix2SD1JGRTtyG24VR821SA8UdxRHgMM_tdzXPfpk/edit <https://docs.google.com/forms/d/1e7fix2SD1JGRTtyG24VR821SA8UdxRHgMM_tdzXPfpk/edit>`_

  - 3 juin à 20h30 en visio : Penser la question juive, une petite histoire de l’antijudaïsme
  - 17 juin à 20h30 en visio : L'antisémitisme économique et l'antisemitisme racial

- 23 juin : `AG de Golem <https://docs.google.com/forms/d/e/1FAIpQLSe5gVPpeqgb5AXefCQ72KWd9Lvd7WLeWwgkwrN6h7Ui6T0S-g/viewform?usp=sf_link>`_


Porte parolat
=================

La campagne de candidatures pour élire les portes-paroles de Golem pour un
mandat de 6 mois est prolongée car nous n'avons pas reçu de candidature.

Modalités
-----------

Nous vous invitons à constituer un binôme de 2 candidats-es qui ne peuvent
être que 2 hommes cisgenres.

Envoyer par mail à collectif_golem@protonmail.com : Nom, prénom et profession de foi
de chaque membre du binôme de candidat·es et en option une vidéo de présentation pour
celles et ceux qui ne vous connaissent pas.

Dans la profession de foi, chaque membre du binôme précise s’il ou elle veut
une exposition médiatique ou préfère un certain anonymat lors d'interventions
écrites ou radio (voir fiche).

Nous suggérons d’élire aujourd’hui une partie des sièges : 4 personnes soit 2 binômes.

- Nous organiserons ensuite une formation media training ouverte aux élu-es et aux
  personnes intéressées par le rôle mais qui n’auraient pas osé se présenter dans un
  premier temps.
- De nouvelles élections seront organisées début juin pour pourvoir les 2 sièges
  restants.

N'hésitez pas à contacter Lorenzo en cas de questions.
Les élections se feront par vote électronique dès que nous aurons reçu des candidatures.


Récap des 15 derniers jours
===============================

Vie des pôles
-------------------

Au sein du pôle action, un groupe de travail sur l’antisémitisme dans le monde
de la culture s’est créé.

Dimanche 19 mai, 12 membres du Service d’Ordre et quelques membres de Golem
se sont formés sur les bons réflexes d'autodéfense à adopter en cas de besoin.

Au programme, échanges sur les bases théoriques et entraînement dans un parc
sous les arbres.

Le service d'ordre (SO) a pour mission d'assurer la sécurité des membres du
collectif lors des actions.
A terme, l'idée serait que tous·tes les membres du collectif puissent être
à l'aise avec l'idée d'assurer un service d'ordre organisé, et être capable
d'adopter les bons gestes en cas de besoin.

Actions - collages
=======================

L’extrême droite fasciste était autorisée à défiler le 11 mai dans
le 5e arrondissement comme chaque année, nous avons décidé de coller la
veille dans le quartier pour rappeler que l’antisémitisme est au coeur du
projet de l’extrême droite.
D’autres actions sur le sujet suivront

Manifestations - rassemblements
===================================

Dimanche 12 mai
------------------

Dimanche 12 mai, quelques membres de Golem ont participé à la diffusion de
la cérémonie du 19e JOINT MEMORIAL DAY CEREMONY organisé par les Guerrières de la Paix,
JCall et leurs partenaires, à l’occasion de Yom Hazikaron.

Mardi 14 mai 2024
-------------------

Mardi 14 mai, plusieurs membres de Golem étaient présents devant le Mur des Justes au
Mémorial de la Shoah pour dénoncer sa profanation.

Vendredi 17 mai 2024
-----------------------

Vendredi 17 mai, Golem étaient rassemblés place de la République pour allumer les
bougies de Shabbat suite à la tentative d'incendie sur la synagogue de Rouen, à l’initiative
de l’UEJF.


Communication
==============

Sur insta
-------------

- Un post insta pour parler de la réunion publique du 23 mai 2024
  https://www.instagram.com/p/C6yZHqyCgtV/

- Un post insta pour le collage contre l’extreme droite
  https://www.instagram.com/p/C61TC9qt2k7/

- Des story en soutien aux mouvements pour un cessez le feu en Israel

Sur twitter
--------------

- Un tweet sur le collage :
  - https://twitter.com/Collectif_Golem/status/1789319657138028999

- Un tweet contre la profanation du Mur des Justes

  - https://x.com/Collectif_Golem/status/1790375324443635783

Communiqués/ tribunes
==========================

Golem a signé la tribune contre l’offensive transphobe contre les droits des
personnes trans, au coté de 800 collectifs

- https://www.politis.fr/articles/2024/04/attaques-contre-les-droits-trans-et-reproductifs-nattendons-plus-faisons-front/

Les membres de Golem ont du talent
========================================

Martine Cohen et Arié Alimi
--------------------------------

- Martine Cohen et Arié Alimi ont échangé dans l’émission "Carrefour du Maghreb" sur
  le thème "les séfarades face à la guerre" https://www.rfi.fr/fr/podcasts/carrefour-du-maghreb/20240504-les-s%C3%A9farades-face-%C3%A0-la-guerre

Arié Alimi
-------------

- Arié Alimi était en dialogue avec Simon Assoun de Tsedek dans l’émission Au Poste
  de David Dufresne
  https://www.youtube.com/watch?v=IoRqEjHq3WQ

Alexandre Journo
---------------------

- Alexandre Journo a récemment publié dans la revue Conditions une lecture critique
  des textes antisionistes rassemblés par Michèle Sibony, Béatrice Orès et Sonia
  Fayman, dans le prolongement des discussions que nous avions pu avoir en visio
  https://revue-conditions.com/traverseesantisionistes

Jonas Pardo
--------------

- Jonas Pardo intervenait sur le twitch de France Info dans un débat
  "L’antisémitisme est-il plus présent en France" au côté de Nona Mayer et Dominique Sopo
  https://www.youtube.com/watch?v=e0VDv4A42Fg

