

.. _raar_golem_2024_05_22:

=====================================================================================================================================================================
2024-05-22 **Communiqué du collectif Golem suite à l'intimidation de deux de leurs militants à l'Université de Lille et la diffamation publique de leur collectif**
=====================================================================================================================================================================

- https://x.com/Collectif_Golem/status/1793331202981577132

Nous étions invités le mardi 21 mai 2024 à une table ronde organisée par la
présidence de l'Université de Lille en présence de Régis Bordet, président
de l'Université de Lille et animée par Pierre Savary, directeur de l'ESJ,
sur le thème Israël-Palestine : Comment une communauté universitaire s'engage
avec la présidente de l'association Academic Solidarity With Palestine et deux
militants de l'Association Libre Palestine.

Nous y allions pour dénoncer la restriction des libertés publiques et la
répression du mouvement étudiant en soutien au peuple palestinien avec
l'interdiction de conférences et les interventions policières dans les
universités.

Nous y allions également pour alerter sur l'antisémitisme dont sont victimes
les étudiants juifs depuis le 7 octobre et la nécessité pour les organisations
syndicales étudiantes, pour le mouvement de solidarité avec la Palestine et
pour l’administration universitaire d'entendre leur souffrance.

Et de garantir aux étudiants juifs des conditions d'études sereines en combattant
l'antisémitisme.

Malheureusement nous n'avons pas pu faire entendre notre voix car nous sommes
tombés dans une véritable embuscade.
Avant même le début de la table ronde, les deux militants de Libre Palestine
ont refusé de nous adresser la parole ce qui n'augure rien de bon dans le
cadre d'une discussion.
Après une présentation rapide du débat par Pierre Savary, une militante de
Libre Palestine a lu un communiqué collectif, **préparé à l'avance avec
l'ensemble de ses camarades nous a t-elle précisé**.

**Dans ce communiqué, il était peu question de solidarité avec le peuple
palestinien**.
Il s'agissait surtout d'une diatribe haineuse contre le collectif Golem que
nous avons dû écouter pendant que plusieurs étudiants nous filmaient avec leur
portable, à l'affut du moment où, face aux calomnies, nous finirions par
perdre notre sang froid.
Après avoir appelé à la décolonisation de toute la Palestine historique,
l'étudiante nous a accusés d’être des partisans de Netanyahu, de soutenir
un génocide à Gaza, d'être des colons, d'être un collectif raciste et antisémite.

Elle n'a pas hésité à expliquer que la lutte contre l'antisémitisme était
une cause noble mais pas dans le cadre du mouvement de solidarité avec la
Palestine.
Nous ne sommes pas antisémites at-elle dit, la preuve, nous aurions
aimé que l'UJFP et Tsedek puissent venir à la place de Golem.
Et puis de toute façon, l'antisémitisme c'est la faute d'Israël at-elle
affirmé en citant Rony Brauman.

À la fin de ces accusations antisémites **que personne n'a essayé de stopper**,
son camarade s'est levé et est venu nous lire, les yeux dans les yeux en nous
pointant du doigt, un poème de Mahmoud Darwish.
Parmi les paroles passagères : "Vous fournissez l'épée, nous fournissons le sang.
Vous fournissez l'acier et le feu, nous fournissons la chair [...] prenez
votre lot de notre sang et partez", nous ‘accusant ouvertement d'avoir du sang
sur les mains, d'être responsables et même partie prenante des massacres à Gaza.

Le public s'est ensuite levé brandissant des drapeaux palestiniens et en nous
hurlant des slogans  "sionistes", "fascistes", "c'est vous les terroristes" ;
"On ne discute pas avec des sionistes", "Vous n'avez pas votre place ici" , "Vous êtes des colons".

Nous n'avons pas pu parler et nous avons dû subir sans broncher les calomnies
antisémites dont on nous a abreuvé.

**Seuls quelques étudiants sont restés jusqu'à la fin pour enfin nous écouter
et nous les en remercions**.

Nous avons plusieurs messages à adresser aux militants de Libre Palestine qui
ont écrit ce communiqué ainsi qu'aux étudiants présents dans le public qui
nous ont traité de fascistes et de colons.

**Avec ce communiqué, pendant cette table ronde, vous avez sombré dans
l'antisémitisme**.

Vous faites l'amalgame entre Juifs, israéliens, sionistes, colons et criminels
de guerre que vous utilisez de manière interchangeable.
En nous accusant d'être complices d'un génocide quand bien même nous avons
toujours dénoncé les massacres à Gaza et appelé à un Cessez-le feu, vous nous
mettez une cible dans le dos et vous encouragez la violence à notre égard.

Vous vous réfugiez derrière Tsedek, l'UJFP et Rony Brauman **pour mieux essentialiser
tous les Juifs qui n’ont pas grâce à vos yeux et les accuser des crimes de
l’armée israélienne dont ils ne sont en rien responsables**.
**Vous perdez l’occasion de soutenir le peuple palestinien et vous préférez
vous attaquer à des étudiants juifs dont le seul tort est de parler d’antisémitisme**.
S'en prendre aux Juifs, qu'ils soient sionistes ou pas, n'aide en rien le
peuple palestinien.

La haine antisémite que vous avez étalée au grand jour pendant cette table
ronde est un désastre pour votre collectif dont vous avez dévoilé l'imposture,
un désastre pour les étudiants juifs qui ne se sentent plus en sécurité
pour étudier, et surtout un désastre pour le mouvement de solidarité avec
le peuple palestinien dont la perméabilité avec l'antisémitisme est un des
principaux freins.

À la présidence de l'Université de Lille, nous voulons dire qu'il n’est
pas normal qu'il n'y ait pas eu de réaction de votre part face à l'agression
antisémite dont nous avons été victimes sous vos yeux, qu'il n'est pas normal
que nous ayons dû supporter les invectives seuls sur l'estrade pendant 20
minutes pendant que vous discutiez avec les étudiants, **qu'il n’est pas normal
que nous nous soyons sentis en danger et humiliés dans le cadre d'un événement
organisé par la présidence de l'Université de Lille**.

Pour revenir au thème de cette table ronde, la communauté universitaire doit
s'engager de deux façons : en garantissant la liberté de réunion, de débat
et de manifester aux étudiants d'une part et d'autre part en garantissant la
sécurité des étudiants face à l'explosion des actes antisémites depuis le
7 octobre.
**Force est de constater qu'aujourd'hui, la communauté universitaire
échoue aussi bien dans un cas que dans l'autre**.

22/05/2024 - Collectif GOLEM


.. figure:: images/page_1.png
.. figure:: images/page_2.png
