.. index::
   pair: Golem; [COMMUNIQUÉ] Cessez de cacher l'antisémitisme sous le tapis Réponse au communiqué de l'Université de Lille (2024-05-28)

.. _golem_com_2024_05_28:

=================================================================================================================================================
2024-05-28 📢 [Golem]  **Cessez de cacher l'antisémitisme sous le tapis Réponse au communiqué de l'Université de Lille** par collectif Golem
=================================================================================================================================================

- https://x.com/Collectif_Golem/status/1795511369397137891

Cessez de cacher l'antisémitisme sous le tapis Réponse au communiqué de l'Université de Lille

Le 23 mai 2024, l'Université de Lille publiait un communiqué pour condamner
le sabotage d'une conférence où intervenait Golem et regrettait l'impossibilité
de tenir un dialogue respectueux.
Lors de cette conférence, **deux représentants de Golem ont été victime d'une
agression antisémite**.
Nous déplorons que dans ce communiqué, il n'y en ait aucune mention, pourtant
flagrant dans cette séquence.
**Une fois de plus, l'antisémitisme se retrouve invisibilisé et caché sous le tapis**.
Or il ne s"agit pas seulement d'une impossibilité de débattre à l'université mais bien
d'une agression antisémite.

Il ne devrait pas être nécessaire de démontrer ce qui relève de
l'évidence. Pour autant, face au déni, nous souhaitons exposer ici en **quoi cette
séquence est évidemment antisémite et appelons la présidence de l'Université
de Lille à sortir de cet épisode navrant par le haut**.

Avant même d'avoir pu prendre la parole, les deux représentants de Golem ont
été traités **de fascistes, de colons, de terroristes par les représentants de
Libre Palestine puis par des étudiants assistant à la table ronde**.
Un étudiant de Libre Palestine leur a lu un poème de Darwish "Vous fournissez l'épée, nous
fournissons le sang. Vous fournissez l'acier et le feu, nous fournissons la chair
[...] Alors prenez votre lot de notre sang, et partez."
La lecture de ce poème, adressé non pas à l'assemblée mais à eux directement,
est une manière de les **tenir personnellement responsables des massacres de
civils à Gaza**. À la demande de la présidence de l'Université de Lille, les
deux représentants de Golem ont été **escortés par les agents de sécurité de
l'Université et ont été exfiltrés par l'arrière du bâtiment universitaire**.

**Personne ne conteste ces faits**, dont il existe un enregistrement vidéo.
Ni l'article de l'AEF du 23 mai, ni l'article de Check News du 23 mai 2024.
Pas même les membres du Comité Palestine de Lille qui ont publié posé de
membres défendant ou non la politique génocidaire de Netanyahu.
Ils assument également la lecture du poème de Darwish aux deux représentants
de Golem.

Alors qu'ils sont personnellement **engagés dans le mouvement de solidarité avec
la Palestine**, que le collectif Golem a, à plusieurs reprises, **dénoncé les
massacres à Gaza et demandé un Cessez-le feu**, les deux représentants de Golem
ont été **assimilés à la propagande de guerre israélienne parce qu'ils sont
Juifs et qu'ils entendent ne pas subir l'antisémitisme** en hausse depuis
le 7 octobre.

Pour ceux qui les menaçaient et les silenciaient, il ne s'agissait pas
de deux Juifs réels mais de Juifs imaginaires, deux entités abstraites, des
supposés agents inféodés à Israël envoyés par on ne sait qui, puisque les
insultes et les reproches ne qualifiaient pas des paroles et des écrits réels,
mais imaginaires. Quoiqu'ils pensent, quoiqu'ils aient pu publier, même s'ils
ont demandé un cessez-le-feu dès la première semaine des bombardements, même
s'ils demandent la reconnaissance d'un État palestinien par Israël, même
s'ils participent régulièrement aux manifestations pour le Cessez-le feu im-
médiat et la libération des otages, **ils sont avant tout des Juifs qui osent
proposer une discussion sur l'antisémitisme**.

Être insulté dans une table ronde **où l'on a pas encore dit un mot** est
déjà inadmissible, mais **être insulté publiquement en tant que Juif,
sans que le personnel académique ou administratif de l’université de Lille
n'intervienne**, relève d'un niveau supérieur. L'Université de Lille doit prendre
ses responsabilités et commencer par appeler un chat un chat. **Lorsqu'on agresse
des Juifs parce qu'ils sont Juifs, il faut écrire le mot antisémitisme dans
son communiqué**.

Il n'y a pas besoin d'attendre l'enquête ou l'analyse vidéo pour qualifier
des faits connus, **que personne ne conteste et dont la dimension antisémite
est évidente**. Ne pas le faire, revient à considérer **qu'on peut traiter
des étudiants juifs de colons, de terroristes sans que ce soit antisémite.
Qu'on peut rendre des étudiants juifs responsables des massacres à Gaza sans
que ce soit antisémite. Qu'on peut être contraint de mobiliser le service de
sécurité universitaire pour exfiltrer des étudiants juifs, parce que leur
sécurité n’était plus garantie, sans que cela ne soit antisémite**.

Ce n'est pas la première affaire d'antisémitisme qui se déroule à l'Université
de Lille depuis le 7 octobre. La réponse des personnels académiques à la
situation extrêmement inquiétante pour les étudiants juifs n'est tout simplement
pas à la hauteur. **Les agressions physiques et verbales sur les campus, le harcèlement
et l'ostracisation des étudiants juifs, l'impossibilité d'étudier sereinement
est une réalité** qui a donné lieu à des échecs scolaires, à des abandons de
cours, à des changements d'orientation ou d'établissement, à des traitements
médicaux et même parfois à des **tentatives de suicide**.

Si l'Université de Lille ne condamne pas l'antisémitisme de manière franche et
sans détour, si elle ne met pas en place des moyens pour définir les limites
et les termes du débat, si elle n'engage pas un travail pédagogique avec ses
étudiantes, l'antisémitisme ne fera qu'augmenter.

Nous demandons que l'Université de Lille rédige un nouveau communiqué dans
lequel elle déclare clairement l'agression antisémite du 21 mai 2024.

Nous demandons que des **campagnes de sensibilisation sur l'antisémitisme soient
mises en place dans tous les établissements d'enseignement supérieur avec:**

- Le recrutement de **référents racisme et antisémitisme** bénéficiant d'une
  formation sur l'an- tisémitisme.
- Des **campagnes d'affichage** contre l'antisémitisme.
- La mise en place de **formations contre l'antisémitisme pour tous les étudiants
  lors des rentrées universitaires** avec des associations spécialisées dans la
  lutte contre l'antisémitisme.

28/05/2024 - Collectif GOLEM

