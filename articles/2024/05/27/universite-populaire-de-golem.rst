

.. _pardo_2024_05_27:
.. index::
   pair: Universite populaire du golem ; 2024-05-27

.. _golem_2024_05_27:

==================================================================================
2024-05-27 **Universite populaire du golem : "Qu'est ce que l'antisémitisme ?"**
==================================================================================

- :ref:`raar_2024:pardo_2024_06_17`
- :ref:`raar_2024:pardo_2024_10_03`

Introduction
================


Qu'est ce que l'antisémitisme ? Conférences sur son histoire par Jonas Pardo
formateur à la lutte contre l'antisémitisme

3 et 17 juin à 20h30 en visio

Inscription : lien en description
ou par mail mouvementgolem@gmail.com

@collectif _golem

Pensées dans un aller retour entre hier et aujourd'hui, ces conférences donneront
des clefs de compréhension de l'antisémitisme contemporain.

Lundi 3 juin 2024
===================

Lundi 3 juin 2024 Jonas Pardo exposera rapidement quelques pistes permettant de penser
la question juive, dont l'aspect religieux n'est qu'une composante parmi d'autres,
et rappellera quelques grandes étapes de l'histoire de l'antijudaïsme et des
persécutions antijuives dans l'occident chrétien.

Lundi 17 juin 2024
========================

Lundi 17 juin 2024 il disséquera d'un côté les théories racialistes forgées
à l'ère des colonisations à l'origine de l'antisémitisme racial, opposition
fantasmée entre « aryens» et « sémites » ; de l'autre côté, les errances de
certaines théories construites parmi les opposants au développement du capitalisme
industriel à l'origine de l'antisémitisme économique, formes personnifiées
de l'anticapitalisme qui ont pu dériver sur le « socialisme des imbéciles ».

Objectifs des deux conférences
==================================

Définir les mots et explorer les notions
----------------------------------------------

« JUIf-ve », « Judaïsme », « Antijudaïsme »,
« Racisme », « Antisémitisme », « Sionisme »,
« Théories du complot »

Connaître les éléments historiques
--------------------------------------

Connaître les éléments historiques permettant de reconnaître l'antisémitisme

Étudier des expressions d'antisémitisme cryptées apparues dans l'espace public
--------------------------------------------------------------------------------

Identifier une théorie complotiste
-----------------------------------------



.. _videos_golem_2024_06_17:

Vidéos Golem
==================

- https://www.youtube.com/@MouvementGolem/videos

1/5 Université populaire du Golem - Introduction à la question juive
--------------------------------------------------------------------------

.. youtube:: QRzMs4jzWiM

   1/5 Université populaire du Golem - Introduction à la question juive

2/5 Université populaire du Golem - Histoire de l'anti-judaïsme chrétien 
---------------------------------------------------------------------------

.. youtube:: JVddiy-xIcs

   2/5 Université populaire du Golem - Histoire de l'anti-judaïsme chrétien 

3/5 Université populaire du Golem - L'antisémitisme racial
---------------------------------------------------------------

.. youtube:: NSsi9yx3VP8

   3/5 Université populaire du Golem - L'antisémitisme racial

4/5 Université populaire du Golem - L'antisémitisme économique
---------------------------------------------------------------------

.. youtube:: YtZO409-BuM

   4/5 Université populaire du Golem - L'antisémitisme économique


5/5 Université populaire du Golem - La théorie du complot juif
--------------------------------------------------------------------

.. youtube:: sOy9mS0A1zU

   5/5 Université populaire du Golem - La théorie du complot juif
