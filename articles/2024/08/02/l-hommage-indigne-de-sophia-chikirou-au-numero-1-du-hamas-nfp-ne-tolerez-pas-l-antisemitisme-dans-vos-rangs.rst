.. index::
   pair: golem ; Lettre ouverte - Collectif GOLEM **L'hommage indigne de Sophia Chikirou au numéro 1 du Hamas NFP : Ne tolérez pas l'antisémitisme dans vos rangs ! (2024-08-02)

.. _golem_2024_08_02:

===========================================================================================================================================================================
2024-08-02 Lettre ouverte - Collectif GOLEM **L'hommage indigne de Sophia Chikirou au numéro 1 du Hamas NFP : Ne tolérez pas l'antisémitisme dans vos rangs !** |golem|
===========================================================================================================================================================================

- https://x.com/Collectif_Golem/status/1819438995606028302
- https://x.com/RaphaAssouline/status/1819063382101651720
- :ref:`antisem:lfi`
- :ref:`antisem:sophia_chikirou`


Introduction
==================

Cher Nouveau Front Populaire,

Nous vous avons soutenu publiquement, nous avons salué l'union de la Gauche
que vous avez réalisée, nous avons tracté aux côtés de vos militant.es
pour battre l'extrême droite, nous avons célébré votre victoire aux
élections législatives, nous avons accueilli avec espoir vos engagements
en termes de lutte contre l'antisémitisme. 

**C’est pourquoi nous vous demandons aujourd'hui de ne pas trahir nos espoirs 
et la confiance que nous vous avons accordée**.


.. _urgence_palestine_2024_08_01:
.. _chikirou_2024_08_01:

Le ler août 2024, Sophia Chikirou, députée du NFP dans le 20ème arrondissement de Paris et cadre de la France Insoumise, a relayée sur son compte Instagram privé un hommage au chef du bureau politique du Hamas Ismaël Haniyeh publié en premier instance par Urgence Palestine
===================================================================================================================================================================================================================================================================================

- :ref:`antisem:sophia_chikirou`
- :ref:`antisem:lfi`

Le ler août 2024, Sophia Chikirou, députée du NFP dans le 20ème
arrondissement de Paris et cadre de la France Insoumise, a relayée sur son
compte Instagram privé un hommage au chef du bureau politique du Hamas
Ismaël Haniyeh publié en premier instance par Urgence Palestine.

Cet hommage intervient 300 jours après les massacres terroristes et les
viols du 7 octobre 2023 commis par le Hamas et le Jihad islamique palestinien,
300 jours durant lesquels des otages israéliens sont toujours en captivité,
300 jours durant lesquels les bombes de l'armée israélienne pleuvent
sur Gaza et massacrent le peuple palestinien, 300 jours durant lesquels la
majorité des infrastructures et des bâtiments de la bande de Gaza ont été
détruits provoquant une crise humanitaire désastreuse, des famines et des
épidémies, dont les effets se feront ressentir sur des dizaines d'années,
300 jours durant lesquels les roquettes du Hezbollah et du Hamas pleuvent
sur Israël f breux morts et réfugiés.

Dans cet hommage, Ismaël Haniyeh est désigné comme "un dirigeant
de la résistance palestinienne mort en martyr pour la dignité de son
peuple" **alors qu'il n'est rien d'autre qu'un criminel de Guerre et l'un des
principaux responsables avec le gouvernement d'extrême droite israélien
de cette Guerre atroce**.


Ismaël Haniyeh était le dirigeant du Hamas, une organisation islamiste d'extrême droite prônant l'établissement d’un État théocratique en Palestine
======================================================================================================================================================

**Ismaël Haniyeh était le dirigeant du Hamas, une organisation islamiste
d'extrême droite prônant l'établissement d’un État théocratique
en Palestine** et plus largement dans l'ensemble du monde. 

L'article 5 de la charte du Hamas affirme "Dieu est son but, l'Apôtre son 
modèle et le Coran sa constitution." 

Sa dimension spatiale : "Partout où se trouvent des musulmans qui adoptent 
l'Islam comme règle de vie, en n'importe quelle partie
de la terre". 

**C'est une organisation antisémite qui prône l’extermination des Juifs dans 
l’article 7 de sa charte** : "l'apôtre de Dieu - que Dieu lui donne bénédiction 
et paix - a dit L'heure ne viendra pas avant que les musulmans n'aient 
combattu les Juifs (c'est à dire que les musulmans ne les aient tué), 
avant que les Juifs ne se fussent cachés derrière les pierres et les arbres 
et que les arbres et les pierres eussent dit : **Musulman, serviteur de Dieu ! 
Un Juif se cache derrière moi, viens et tue-le.**"


Le 7 octobre 2023 était une attaque terroriste qui n'avait rien à voir avec de la résistance
================================================================================================

Le 7 octobre 2023 était une attaque terroriste qui n'avait rien à voir avec de la
résistance sauf à considérer que le massacre de civils et le viol sont des
actes de résistance. 

Dans son discours du 7 octobre 2023 sur Al Jazeera, Ismaël Hanyieh revendique 
l'attaque terroriste du 7 octobre au cours de laquelle 1175 personnes ont 
été massacrées, dans un discours le 14 octobre, il salue le courage et 
l'héroïsme des assassins et des violeurs qui ont participé à l'attaque 
terroriste du 7 octobre qu'il qualifie de "jour glorieux". |


En repartageant cet hommage d'Urgence Palestine à Ismaël Hanyieh, Sophia Chikirou rend hommage à un meurtrier responsable de la mort de milliers d'Israéliens et de Palestiniens
===================================================================================================================================================================================

En repartageant cet hommage d'Urgence Palestine à Ismaël Hanyieh, **Sophia
Chikirou**:

- **rend hommage à un meurtrier responsable de la mort de milliers d'Israéliens et de Palestiniens**, 
- elle **rend hommage à un islamiste d'extrême droite**, 
- elle **rend hommage à un criminel de guerre mise en examen par la CPI pour 
  crime de guerre et crime contre l'humanité**.

En reprenant le terme de Martyr, Sophia Chikirou s'inscrit à la suite
d'Urgence Palestine dans la logique mortifère du Hamas qui l'a poussé
a déclencher une guerre meurtrière avec Israël sans se soucier des
répercussions sur les civils palestiniens, qui n'hésite pas à se servir
de son peuple comme bouclier humain et lui interdit de se réfugier dans
les tunnels pour se protéger, comme l'a rappelé Mousa Abu Marzouk,
membre du bureau politique du Hamas, dans une interview le 30 octobre 2023 :
"Chaque palestinien est un martyr en puissance que le Hamas est prêt à
sacrifier pour servir ses objectifs politiques islamistes millénaristes". 

En partageant cet hommage d'Urgence Palestine à Ismaël Hanyieh, Sophia
Chikirou **crache sur la mémoire des victimes israéliennes assassinées
le 7 octobre** et sur la mémoire des **victimes palestiniennes tuées depuis
le 7 octobre** et le déclenchement de la Guerre. 

Elle crache **sur la mémoire des 42 citoyens français assassinés le 7 octobre 
et encore en captivité à Gaza**. 

Elle se pose en soutien du terrorisme, en soutien des forces obscurantistes 
qui ont plongé le Moyen-Orient dans l'horreur, elle se pose en ennemi de 
la paix, de la justice et des forces progressistes israéliennes et 
palestinienne qui oeuvrent pour le cessez le feu, pour la libération des 
otages et pour une paix juste et durable entre peuple palestinien et israélien.

Elle crache également sur ses alliées du Nouveau Front Populaire et sur
son programme qui l'engageait pourtant à "**agir pour la libération des
otages** détenus depuis les massacres terroristes du Hamas, dont nous rejetons
le projet théocratique, et pour la libération des prisonniers politiques
palestiniens", à "soutenir la Cour Pénale internationale dans ses poursuites
contre les dirigeants du Hamas et le gouvernement de Netanyahu” et à
**"reconnaître immédiatement l’État de Palestine aux côté de l’État
d'Israël sur la base des résolutions de l'ONU"**. 

Elle crache sur les militants qui ont comme nous participé à la campagne 
législative du Nouveau Front Populaire contre l'extrême droite sur la base 
de ce programme. 

**Elle crache sur toutes les valeurs de gauche et se vautre dans la honte 
et le déshonneur**.

Si le Nouveau Front Populaire veut que sa parole reste crédible, s'il veut
que ses promesses soient prises au sérieux, s’il veut que nous ayons
confiance dans sa volonté de respecter le programme pour lequel il a été
élu, s'il ne veut pas que la honte et le déshonneur de Sophia Chikirou
rejaillissent sur l'ensemble de ses membres, **nous demandons à tous les
formations politiques du NFP de ne pas rester silencieux et de condamner
fermement, unanimement et publiquement les dérives antisémites de
cette député** qui a prouvé qu'elle n'avait plus sa place au sein de la
gauche. 

Nous demandons également que le **NFP prenne ses distances avec
les formations politiques qui refuseraient de condamner le partage de cet
hommage par Sophia Chikirou et en tire toutes les conséquences**.  

L'union de la gauche ne peut se faire au détriment de la lutte contre 
l'antisémitisme, au mépris des valeurs de gauche et sans projet politique commun.

Dans leur charte d'engagement républicain contre l'antisémitisme,
le Parti socialiste, Place Publique, le Parti Communiste Français et
Les Écologistes déclaraient "**nous engageons nos formations politiques
à sanctionner immédiatement tout candidat ou élu proférant des propos
antisémites ou des propos relativisant l'antisémitisme**”. 

Voici l'occasion de prouver que votre parole a une valeur, que vos 
engagements ne sont pas du vent, que votre lutte contre l'antisémitisme 
est sincère.

**Vous n’en sortirez que grandis et la lutte contre l'extrême droite n’en
sera que renforcé tant l'antisémitisme qui gangrène une partie de la
gauche a constitué un frein à la création du Nou- veau Front Populaire
et à la campagne des législatives**.

02/08/2024 :ref:`Collectif GOLEM <golem>` |golem|

