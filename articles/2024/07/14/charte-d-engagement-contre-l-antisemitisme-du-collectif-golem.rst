.. index::
   pair: Charte ; Golem (2024-07-14)

.. _charte_golem_2024_07_14:

========================================================================================
2024-07-14 **Charte d'engagement contre l'antisémitisme** du collectif golem |golem|
========================================================================================


Annonce 
==========

Charte d'engagement contre l'antisémitisme du collectif Golem

Nous appelons les organisations et les élu•es du #NouveauFrontPopulaire  
à prendre des engagements concrets pour lutter contre l'antisémitisme 
et à organiser des formations sur l'antisémitisme pour ses membres

.. figure:: images/le_message.webp
   
   https://x.com/Collectif_Golem/status/1812581676008063218


Préambule
=============

Nous, militantes juifves et alliées de gauche, antiracistes et antifascistes,
affirmons que nous sommes soulagé·es par la victoire du Nouveau Front Populaire
(NFP) aux élections législatives. 

Nous soutenons pleinement les efforts du NFP pour lutter contre toutes les 
discriminations, et pour faire barrage à l'extrême droite. 

Nous saluons le NFP pour les différentes chartes contre l'antisémitisme 
proposées par ses partis membres, qui incluent univoquement la condamnation 
ferme de l'antisémitisme, la sécurité des lieux de culte, la formation 
des jeunes et des élu·es, ainsi que des plans de lutte contre les discriminations.

Nous pensons qu'il est crucial de renforcer ces chartes par des clarifications
afin de dissiper la crise de confiance qui nous touche à gauche sur cette question

Nous restons **profondément préoccupé·es par les tentatives de récupération
de l'extrême droite qui instrumentalisent les failles de la gauche**, cherchant
à l'affaiblir, alors même que ce parti reste le premier danger pour toutes les
minorités de France.

**Ces tentatives contribuent aux logiques de division et d'opposition entre 
les communautés**. 

Aujourd'hui, le NFP a une opportunité unique et le devoir de réconcilier 
les communautés autour d'un projet fédérateur social et écologique, en 
soignant les plaies et en créant un espace politique inclusif et solidaire.

Cette charte, bien que centrée sur l'antisémitisme, a vocation à s'inscrire
**dans un combat antiraciste global**, incluant des mesures contre tous 
les racismes et discriminations. 
Chaque racisme a ses spécificités, et il est crucial de les aborder de 
manière cohérente. 
Néanmoins, nous nous concentrerons ici sur l'antisémitisme, domaine sur 
lequel nous estimons pouvoir apporter une contribution pertinente.

Reconnaissance du contexte social et politique
==================================================

L'antisémitisme en France connaît une recrudescence alarmante, avec une
augmentation de 300% des faits antisémites de janvier à mars 2024 par 
rapport à 2023 (chiffres du gouvernement, Le Monde).

En 2023, plus de la moitié des faits à caractère raciste ou antireligieux
recensés étaient antisémites, alors que les Juifves ne représentent que 
0,5% de la population française (CNCDH). 
Les récentes actualités, marquées **par des actes antisémites violents et 
des propos ridiculisant la lutte contre l'antisémitisme, illustrent un 
climat inquiétant de haine antijuive**

.. figure:: images/page_1.webp


L‘ambiguïté des discours et leurs conséquences néfastes pour les Juifves en France
====================================================================================

La préoccupation des Juifves français·es ne résulte pas uniquement de formes
explicites de haine dirigées contre eux. 
Si récemment des slogans tels que "mort aux Juifs" lors de manifestations 
et des croix gammées dans les espaces publics ont été observés, d'autres 
discours plus subtils gagnent la sphère publique et sont vécus comme 
hostiles par les Juifves.

Un exemple est l'utilisation fréquente et ambiguë du mot "sioniste". 
Ce terme est ambivalent, sa définition variant selon l'émetteur et le 
récepteur. En effet, selon une étude de l'IFOP (2014), 25% des Français·es 
considèrent le sionisme comme "une organisation visant à influencer le monde 
au profit des Juifves", véhiculant ainsi une connotation complotiste, 
tandis que 46% le voient comme "l'idéologie revendiquant l'existence de 
l'État d'Israël"


Ambiguité sur la signification du mot "sioniste"
-----------------------------------------------------

Malgré cette ambivalence, l'utilisation du terme "sioniste" est devenue
omniprésente dans les discours politiques et militants :

- "Sionistes hors de nos facs’", "A bas les sionistes", "1 sioniste = 1 balle", 
  "Nique un facho, Nique un sioniste" sont des slogans que nous avons vu 
  lors de manifestations.

- Certains élus et représentants de partis utilisent le mot "sioniste"
  pour qualifier indifféremment des élus juifs, des bombes de l'armée 
  israélienne ("bombes sionistes"), ou la guerre au Moyen-Orient ("guerre sioniste"). 

- Dans certaines sphères politiques, le terme "sioniste" est utilisé pour
  véhiculer les poncifs antisémites classiques du 20ème siècle. 
  Ce ne sont plus les Juifves qui sont accusé.es de dominer le monde, 
  mais les sionistes"

- Enfin, des listes de "sionistes" circulent ‘sur Instagram

Cette ambiguité sur la signification du mot sioniste", son utilisation excessive
dans les discours politiques et les slogans, **et le fait qu'il soit rarement
défini sont vécus comme dangereux par les Juifves**. 

35% des Français·es de 18-24 ans estiment qu'il est légitime de s'en prendre à une personne juive en raison de son soutien supposé ou réel au gouvernement israélien
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Ces discours ambigus nourrissent des amalgames. Ainsi, selon une étude de 
l'IFOP (2024), 35% des Français-es de 18-24 ans estiment qu'il est légitime 
de s'en prendre à une personne juive en raison de son soutien supposé 
ou réel au gouvernement israélien.

C'est ainsi dans un esprit d'apaisement que nous faisons nos suggestions pour
renforcer les efforts déjà amorcés.  Bien que nous n'ayons pas la prétention
d'analyser parfaitement les intentions ou les conséquences de ces discours,
nous sommes convaincus que les engagements que nous proposons contribueront 
à calmer les tensions et à redonner un climat de sécurité et de sérénité pour les
Juifves en France.

Propositions d'engagements individuels
=============================================

Nous demandons aux individus et aux groupes politiques de prendre les 
engagements suivants :

Faire preuve de vigilance dans le vocabulaire
------------------------------------------------------

Clarifier l'ambivalence des termes "sionistes"/"antisionistes" 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Clarifier l'ambivalence des termes "sionistes"/"antisionistes" : Éviter au
maximum d'utiliser les mots "sioniste" ou l'antisioniste" dans les prises de
parole publiques. Si leur utilisation est nécessaire, préciser systématiquement
la définition employée pour éviter toute confusion Reconnaître que le terme
"sioniste" est souvent utilisé par des groupes antisémites pour véhiculer des
théories du complot, notamment celles insinuant une domination mondiale 
par les Juifves, et qu'il nourrit des amalgames entre les Juifves et les
actions d'Israël, contribuant ainsi à un climat d'hostilité. Cette utilisation
est d'autant plus ambiguë, que le NFP soutient dans son programme une solution
à deux États pour une paix durable au Moyen-Orient.

.. figure:: images/page_2.webp

S'informer et éliminer l'utilisation de "dog whistles"
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

S'informer et éliminer l'utilisation de "dog whistle" : S'informer sur
les termes codés ou indirects qui véhiculent des idées ‘antisémites (aussi
appelés :term:`dog whistles <antisem:dog whistle>`), même de manière 
inconsciente. Ne plus utiliser ces termes une fois leur ambiguïté et 
leur potentiel offensant compris.


Reconnaître le Hamas comme étant une organisation antisémite
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Reconnaître le Hamas comme étant une organisation antisémite : Reconnaître
que la charte du Hamas de 1988 est ‘antisémite, théocratique, et obscurantiste
et effectuer une clarification systématique de qui est désigné lorsque l'on
parle de "Résistance Palestinienne" pour éviter toute ambiguïté.

Refuser la mise en concurrence des luttes
-----------------------------------------------

Promouvoir la complémentarité des luttes
++++++++++++++++++++++++++++++++++++++++++++

**Promouvoir la complémentarité des luttes** : Ne pas opposer la lutte
contre l'antisémitisme à celle contre l'islamophobie ou au soutien au peuple
palestinien.  
Reconnaître que **ces combats doivent être complémentaires et non concurrents**.


Respecter la singularité des mémoires
++++++++++++++++++++++++++++++++++++++++

Respecter la singularité des mémoires La mémoire n'est pas un jeu à
somme nulle.  
Ne mettons pas en concurrence les dizaine de milliers de civils palestiniens 
tués par l'armée israélienne et la mémoire de la Shoah. 
Ce sont deux événements singuliers avec des dynamiques qui leur sont propres 
et qui doivent être respectés dans leur singularité.

Lutter contre l'antisémitisme d'où qu'il vienne
----------------------------------------------------

Refuser la minimisation de l'antisémitisme
+++++++++++++++++++++++++++++++++++++++++++++

Refuser la minimisation de l'antisémitisme : Ne pas minimiser ou nier
l'importance des faits antisémites. Comme pour toute autre lutte progressiste,
prendre au sérieux les accusations et écouter attentivement le ressenti des
victimes d'antisémitisme.


En finir avec la rhétorique du "rayon paralysant"
+++++++++++++++++++++++++++++++++++++++++++++++++++

**En finir avec la rhétorique du "rayon paralysant"** : Ne pas faire de procès
d'intention en laissant entendre que toute accusation d'antisémitisme qui serait
faite à la gauche cacherait des motivations politiques cachées (théorie dite
du "rayon paralysant"'). 
Traiter les accusations de manière indépendante et objective

Dénoncer l'antisémitisme d’où qu'il vienne
+++++++++++++++++++++++++++++++++++++++++++++++

**Dénoncer l'antisémitisme d’où qu'il vienne** : La gauche n'est pas exempte
d'oppressions. Il faut dénoncer l'antisémitisme quelle que soit sa provenance,
y compris de la gauche, ou des groupes progressistes. Abandonner la lutte contre
l'antisémitisme, sous prétexte que l'antisémitisme est instrumentalisé par les
partis réactionnaires, affaiblit la gauche et la rend vulnérable aux attaques
des autres partis. de

Combattre les préjugés et les accusations infondées
----------------------------------------------------------

Rejeter le trope de "double allégeance" et la "chasse aux sionistes"
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Rejeter le trope de "double allégeance" et la "chasse aux sionistes" :
Ne pas accuser les politicien-nes juifves français-es de loyauté partagée entre
la France et Israël, ni les citoyen.ne.s juifves français de responsabilités
imaginaires dans les actions du gouvernement israélien. 
**Juger les individus sur leurs actions, non sur des intentions présumées**.

**Respecter les groupes politiques juifs**
++++++++++++++++++++++++++++++++++++++++++

Respecter les groupes politiques juifs: Ne pas considérer que le fait de 
lutter contre l'antisémitisme ou de soutenir l'existence de l'État d'Israël 
implique d'être d'extrême droite ou d'être responsable des actions du 
gouvernement israélien.


Vérifier et présenter exhaustivement les informations avant de les diffuser
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

**Vérifier et présenter exhaustivement les informations avant de les diffuser**
:Ne pas propager des informations non- vérifiées ou des fausses nouvelles
("fake news"). 
S'assurer de présenter une image complète des faits, sans sélectionner 
uniquement ceux qui soutiennent un point de vue particulier.

.. figure:: images/page_3.webp


Propositions d'engagements politiques
==========================================

Nous suggérons aux membres des groupes politiques d'adopter les engagements
suivants :

**Encourager le dialogue intercommunautaire**
----------------------------------------------

**Encourager le dialogue intercommunautaire** : faut créer des ponts et 
non des murs. Soutenir et financer des initiatives visant à encourager
le (dialogue entre différentes communautés.  
Organiser des événements et des forums de discussion pour promouvoir la 
compréhension mutuelle et la cohésion sociale.


**Défendre un plan de lutte contre l'antisémitisme**
-----------------------------------------------------

**Défendre un plan de lutte contre l'antisémitisme** : Défendre avec vigueur
un plan de lutte contre l'antisémitisme avec des fonds, prioritairement au
sein des écoles et des universités, pour éduquer les jeunes générations à
la question de l'antisémitisme, aux formes qu'il prend et aux ravages qu'il
cause. 
Ce programme doit s'inscrire dans une démarche globale de sensibilisation
aux questions de diversité et de discrimination.


**Prendre des mesures vis-à-vis de celleux qui profèrent des propos antisémites**
-----------------------------------------------------------------------------------

**Prendre des mesures vis-à-vis de celleux qui profèrent des propos antisémites** :
Exercer une vigilance rigoureuse à l'égard des candidatres, élu·es, groupes,
et collectifs qui tiennent des propos antisémites, en condamnant fermement de
telles sorties et en prenant des mesures appropriées contre celleux qui les
émettent.

**Suivre une formation dédiée contre l'antisémitisme**
-------------------------------------------------------

**Suivre une formation dédiée contre l'antisémitisme** : Chaque groupe
parlementaire s'engage à suivre une formation dédiée à la lutte contre
l'antisémitisme et à l'identification de ses manifestations.

**Mettre en place une formation pour l'administration**
---------------------------------------------------------

Mettre en place une formation pour l'administration : Soutenir la mise en place
d'un programme de formation pour l'administration, les forces de l'ordre et les
agents du service public judiciaire afin de garantir une réponse appropriée et
éclairée face à l'antisémitisme.

**Augmenter la veille contre les discours de haine et "fake news" sur les réseaux sociaux**
--------------------------------------------------------------------------------------------

Augmenter la veille contre les discours de haine et "fake news" sur les
réseaux sociaux : Mettre en place une équipe dédiée pour suivre et examiner
les contenus en ligne, collaborer avec les plateformes pour signaler et retirer
les contenus haineux ou trompeurs, lancer des campagnes de sensibilisation pour
éduquer le public, renforcer les sanctions contre les diffuseurs de haine et de
fausses informations, et publier des rapports réguliers sur l'état des discours
de haine et des "fake news".

**Conclusion**
================

Nous reconnaissons les efforts déjà réalisés par le NFP et saluons les
différentes chartes contre l'antisémitisme proposées par ses partis membres. 

En y ajoutant ces engagements concrets, nous espérons non seulement renforcer 
la lutte contre l'antisémitisme, mais aussi restaurer la confiance. 

Nous avons l'espoir que ces actions essentielles fédéreront nos forces 
et bâtiront une société plus solidaire et résolument contre toutes les 
formes de racisme et de discrimination

.. figure:: images/page_4.webp
